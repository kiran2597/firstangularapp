import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EventsListComponent} from './events/events-list.component';
import { EventsAppComponent } from './events-app.component';
import { EventThumbnailComponent } from './events/events-thumbnail.component';
import { NavBarComponent } from './nav/navbar.component';
import { EventService } from './shared/event-service';
import { EventDetailsComponent } from './events/event-details/event-details.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { CreateEventComponent } from './events/create-event.component';
import { Error404Component } from './errors/404Error.component';
import { EventRouteActivator } from './events/event-details/event-router-activator.service';
import { EventListResolver } from './events/event-list-resolver.service';
import { AuthService } from './user/auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateSessionComponent } from './events/event-details/create-session.component';
import { SessionListComponent } from './events/event-details/session-list.component';
import { CollapsibleComponent } from './common/collapsible.component';
import { JQ_TOKEN } from './common/jQuery.service';
import { SimpleModalComponent } from './common/simpleModal.component';
import { ModalTriggerDirective } from './common/modalTrigger.directive';



let jQuery = window['$']
@NgModule({
  declarations: [
    EventsAppComponent,
    EventsListComponent,
    EventThumbnailComponent,
    EventDetailsComponent,
    NavBarComponent,
    SimpleModalComponent,
    CreateEventComponent,
    Error404Component,
    CreateSessionComponent,
    SessionListComponent,
    ModalTriggerDirective,
    CollapsibleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    EventService,
    EventRouteActivator,
    EventListResolver,
    AuthService,
    { provide:'canDeactivateCreateEvent' ,useValue:checkFormSubmit },
    { provide:JQ_TOKEN, useValue:jQuery }
  ],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }

export function checkFormSubmit(component: CreateEventComponent){

    if(component.isformSubmit){
      return window.confirm('You have not saved the event, do you really want to cancel?')
    }
    return true
}
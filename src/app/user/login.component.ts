import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { AuthService } from './auth.service';


@Component({

    templateUrl:'./login.component.html',
    styles:[`
        em {float:right;color:#FF0000;padding-left:20px}
    `]
})
export class LoginComponent{

    userName
    password
    constructor(private router:Router,private authService:AuthService){

    }
    login(formValue){
        this.authService.loginUser(formValue.userName,formValue.password)
        this.router.navigate(['events'])
    }

    cancel(){
        this.router.navigate(['events'])
    }
} 
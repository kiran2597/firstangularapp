import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { userRoutes } from './user.routes';
import { ProfileComponent } from './profile.component';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
    imports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(userRoutes)
    ],
    declarations:[
        ProfileComponent,
        LoginComponent
    ],
    providers:[
        {provide:'canDeactivateEditProfile',useValue: checkProfileEdit }
    ]
})
export class UserModule{
}

export function checkProfileEdit(component:ProfileComponent){

    if(component.isProfileEdit){
       return window.confirm("Are you sure not to update your profile");
    }
    return true;
}
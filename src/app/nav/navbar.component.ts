import { Component } from '@angular/core'
import { AuthService } from '../user/auth.service';
import { EventService } from '../shared/event-service';
import { IEvent, ISession } from '../shared/event.model';
import { filter } from 'rxjs/operators';
import {  Router, NavigationEnd } from '@angular/router';
@Component({
    selector: 'nav-bar',
    templateUrl: './nav-bar.component.html',
    styles: [`
        .nav.navbar-nav {font-size: 15px;}
        #searchForm {margin-right: 100px;}
        li > a.active {color: #FF5733}
    `]
})
export class NavBarComponent{

    searchTerm:string =""
    eventList: IEvent[]=[]
    foundSessions: ISession[];    

    constructor(private auth:AuthService,private eventService:EventService,private router:Router){    
       this.eventList = this.eventService.events 
    }   

    searchSessions(searchTerm){
        this.eventService.searchSessions(searchTerm).subscribe(
            sessions =>{
                this.foundSessions = sessions
                console.log(this.foundSessions)
            }
        )
        
    }
}
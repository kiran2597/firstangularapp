import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { EventService } from '../shared/event-service';


@Component({
    templateUrl: './create-event.component.html',
    styles: [`
      em {float:right; padding-left:10px; color:#E05C65}
      .error input { background-color: #E3C3C5}
  `]
})
export class CreateEventComponent{

    newEvent
    isformSubmit:boolean =true
    constructor(private router:Router,private eventService:EventService){

    }
    cancel(){
        this.router.navigate(['/events'])
    }

    saveEvent(formValues){
       this.eventService.saveEvent(formValues)
       this.isformSubmit=false
       this.router.navigate(['events'])
    }
}